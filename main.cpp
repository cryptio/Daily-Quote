#include <iostream>
#include <string>
#include "curl/curl.h"
#include <regex>


static size_t write_callback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}


std::string daily_quote()
{
    const std::string url = "http://www.brainyquote.com/quote_of_the_day";
    CURL* c = curl_easy_init();
    CURLcode code;
    std::string buf;
    const std::regex pat("<img class=.+alt=([^>]+)");
    typedef std::match_results<std::string::const_iterator> smatch;
    smatch matches;

    if(c)
    {
        curl_easy_setopt(c, CURLOPT_URL, url.c_str()); // the url
        curl_easy_setopt(c, CURLOPT_FOLLOWLOCATION, 1L);    // follow redirects
        curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);   // string conversion
        curl_easy_setopt(c, CURLOPT_WRITEDATA, &buf);  // write data to string
        code = curl_easy_perform(c);   // perform the GET request and get the data
        curl_easy_cleanup(c);  // clean up

        if(std::regex_search(buf, matches, pat))
        {
            return matches[1];
        }
    }

    return " ";

}


int main()
{
    std::cout << daily_quote() << '\n';

    return 0;
}